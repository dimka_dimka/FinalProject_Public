<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 30.06.2016
  Time: 1:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Information about film</title>
</head>
<body>
<ul>
    <li style="width: 12%;">${movie.duration}</li>
    <li style="width: 12%;">${movie.genre}</li>
    <li style="width: 12%;">${movie.description}</li>
    <li style="width: 12%;">${movie.rating}</li>
    <li style="width: 12%;">${movie.rentStart}</li>
    <li style="width: 12%;">${movie.rentEnd}</li>
</ul>
<p></p>
<h2><p>Список сеансов</p></h2>
<ul>
    <c:forEach items="${sessionDTOs}" var="sessions">
       <p><a href="${pageContext.servletContext.contextPath}/session?id=${sessions.id}">${sessions.time}</a>  Цена билета: ${sessions.price} грн</p>
    </c:forEach>
</ul>
</body>
</html>
