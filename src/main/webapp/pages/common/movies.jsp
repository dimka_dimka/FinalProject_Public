<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>List of movies</title>
</head>
<body>
<center>
    <h1>СПИСОК ФИЛЬМОВ:</h1>
    <br>
        <c:forEach items="${movieDTOList}" var="movies">
            <a href="${pageContext.servletContext.contextPath}/movie?id=${movies.id}">${movies.title}</a><br/>
        </c:forEach>
</center>
<br><br><br>
<form name="name" action="${pageContext.servletContext.contextPath}/UserCabinet">
    <input type="submit" value="Личный кабинет">
</form>
<br><br>
</body>
<jsp:include page="login.jsp"></jsp:include>
</html>
