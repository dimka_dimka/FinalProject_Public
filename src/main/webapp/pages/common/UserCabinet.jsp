<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 13.07.2016
  Time: 18:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%! int number = 1; %>
<html>
<head>
    <title>Личный кабинет пользователя</title>
</head>
<body>
<center>
<h1>ВАШ ЛИЧНЫЙ КАБИНЕТ</h1>
</center>
<h3>${user.firstName} ${user.lastName}</h3>
<h5>Логин: ${user.login}</h5>
<h5>Дата рождения: ${user.birthday}</h5>
<h5>Email: ${user.email}</h5>
<br>
<h5>Список Ваших билетов: </h5>
<c:forEach items="${Tickets}" var="place">
    <p>Билет № <%= number %> </p>
    <p>Время начала сеанса: ${place.session.time} </p>
    <p>Ряд ${place.rowNumber}</p>
    <p>Место ${place.seatNumber}</p>
    <br><br>
    <% number++; %>
</c:forEach>
<br>
<form name="name" action="${pageContext.servletContext.contextPath}/exit">
    <input type="submit"
           name="exit" value="Выход">
</form>
</body>
</html>
