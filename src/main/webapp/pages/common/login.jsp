<%@ page import="dto.UserDTO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
<h3><c:out value="${sessionScope.message}"/></h3>
<h2><c:out value="${sessionScope.user.firstName}" /></h2>
<c:if test="${sessionScope.user == null}">
<form name="loginForm" method="post" action="${pageContext.servletContext.contextPath}/login">
    Username: <input type="text" name="login"/> <br/>
    Password: <input type="password" name="password"/> <br/>
    <input type="submit" value="Login" />
</form>
<p>Или можете <a href="pages/common/registration.jsp">зарегистрироваться</a></p>
</c:if>
<c:if test="${sessionScope.user != null}">
    <form name="name" action="${pageContext.servletContext.contextPath}/exit">
        <input type="submit"
               name="exit" value="Выход">
        </form>
</c:if>
</body>
</html>
