<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 05.07.2016
  Time: 0:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%! int place = 1; %>
<%! int rowNumber = 1; %>
<html>
<head>
    <title>Sessions</title>
</head>
<body>
<c:out value="${sessionScope.message}"/>
<div align="center">
<h1>СХЕМА ЗАЛА</h1>
    <h4>Пожалуйста, выбирите место</h4>
    <form name="order" action="${pageContext.servletContext.contextPath}/BuyTicket">
<c:forEach items="${rowDTOs}" var="row">
    <% place = 1; %>
    <c:forEach begin="1" end="${row.seatQuantity}">
    <input type="checkbox" name="check" value="<%= rowNumber + "/" + place %>" unchecked>
        <% place++; %>
    </c:forEach>
    <% rowNumber++; %>
    <br>
</c:forEach>
    <input type="submit"
           name="save" value="Сделать заказ">
    </form>
</div>
</body>
</html>
