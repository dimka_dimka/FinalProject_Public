<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 15.07.2016
  Time: 3:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Add Sessoin</title>
</head>
<body>
<form name="name" action="${pageContext.servletContext.contextPath}/addSession">
    <h4>Время начала сеанса:</h4>
    <input type="time" name="time" required>
    <h4>Цена билета:</h4>
    <input type="text" name="price" required>
    <h4>Выберите зал:</h4>
    <select type="text" name="hallName" required>
        <option value selected>Зал</option>
        <option value="blue_hall">Синий</option>
        <option value="red_hall">Зелёный</option>
    </select>
    <br>
    <input type="submit" value="Сохранить">
</form>
</body>
</html>
