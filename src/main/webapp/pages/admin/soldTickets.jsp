<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 19.07.2016
  Time: 12:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%! int number = 1; %>
<html>
<head>
    <title>Проданые билеты</title>
</head>
<body>
<h2>Список проданых билетов</h2>
<c:forEach items="${AllTickets}" var="place">
    <p>Билет № <%= number %> </p>
    <p>Фильм: ${place.session.movie.title} </p>
    <p>Время начала сеанса: ${place.session.time} </p>
    <p>Ряд ${place.rowNumber}</p>
    <p>Место ${place.seatNumber}</p>
    <br><br>
    <% number++; %>
</c:forEach>
<br>
<form name="name" action="${pageContext.servletContext.contextPath}/exit">
    <input type="submit"
           name="exit" value="Выход">
</form>
</body>
</html>
