<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 14.07.2016
  Time: 5:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Information about film</title>
</head>
<body>
<ul>
    <li style="width: 12%;">${sessionScope.movie.duration}</li>
    <li style="width: 12%;">${sessionScope.movie.genre}</li>
    <li style="width: 12%;">${sessionScope.movie.description}</li>
    <li style="width: 12%;">${sessionScope.movie.rating}</li>
    <li style="width: 12%;">${sessionScope.movie.rentStart}</li>
    <li style="width: 12%;">${sessionScope.movie.rentEnd}</li>
</ul>
<p></p>
<h2><p>Список сеансов</p></h2>
<br>
<ul>
    <c:forEach items="${sessionDTOs}" var="sessions">
        <tt>${sessions.time}  Цена билета: ${sessions.price} грн   </tt>
        <form name="name" action="${pageContext.servletContext.contextPath}/deleteSession">
            <input type="hidden" name="id" value=${sessions.id}>
            <input type="submit" value="Удалить">
        </form>
        <br>
    </c:forEach>
</ul>
<form name="name" action="${pageContext.servletContext.contextPath}/pages/admin/addSession.jsp">
    <input type="submit" value="Добавить сессию">
</form>
<br>
<form name="name2" action="${pageContext.servletContext.contextPath}/deleteMovie">
    <input type="submit" value="Удалить фильм">
</form>
</body>
</html>