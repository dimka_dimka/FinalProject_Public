<%--
  Created by IntelliJ IDEA.
  User: dmitr
  Date: 13.06.2016
  Time: 17:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Админ</title>
</head>
<body>
<center>
<h1>Admin page</h1>
    <c:out value="${sessionScope.message}"></c:out>
</center>
<h3>Список всех фильмов:</h3>
<c:forEach items="${sessionScope.movieDTOList}" var="movies">
    <li><a href="${pageContext.servletContext.contextPath}/movieAdmin?id=${movies.id}">${movies.title}</a><br/></li>
</c:forEach>
<br>
<form name="name" action="${pageContext.servletContext.contextPath}/pages/admin/addMovie.jsp">
    <input type="submit" value="Добавить фильм">
</form>
<br>
<form name="name" action="${pageContext.servletContext.contextPath}/soldTickets">
    <input type="submit"
           name="exit" value="Проданые билеты">
</form>
<br>
<form name="name" action="${pageContext.servletContext.contextPath}/exit">
    <input type="submit"
           name="exit" value="Выход">
</form>
</body>
</html>
