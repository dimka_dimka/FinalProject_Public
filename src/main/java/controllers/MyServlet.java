//package controllers;
//
//import dao.impl.CrudDAO;
//import dao.impl.MovieDaoImpl;
//import dao.impl.RowDaoimpl;
//import model.Movie;
//import model.Row;
//import service.impl.MovieServiceImpl;
//
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.io.PrintWriter;
//
///**
// * Created by Евгений on 08.06.2016.
// */
//@WebServlet(name = "MyServlet", urlPatterns = {"/"})
//public class MyServlet extends HttpServlet {
//    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        CrudDAO<Movie> movieDao = new MovieDaoImpl(Movie.class);
//        CrudDAO<Row> rowDao = new RowDaoimpl(Row.class);
//
////        response.setContentType("text/html;charset=UTF-8");
//        //        request.setCharacterEncoding("UTF-8");
//        response.setCharacterEncoding("UTF-8");
//        PrintWriter out = response.getWriter();
//        out.print("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">");
//        out.print("<html><head><title>Cinema</title></head><body>");
//        out.print("<h2>Movies</h2>");
//        out.print("<table width=\"100%\" border=\"1\" cellspacing=\"5\" cellpadding=\"5\">");
//        out.print(" <th>Название</th>\n" +
//                "        <th>Длительность</th>\n" +
//                "        <th>Жанр</th>\n" +
//                "        <th>Описание</th>\n" +
//                "        <th>Рейтинг</th>\n" +
//                "        <th>Дата начала проката</th>\n" +
//                "        <th>Дата конца проката</th>");
//
//        for (Movie movie : movieDao.getAll()) {
//            out.print("<tr align = \"center\">");
//            out.print("<td>" + movie.getTitle() + "</td>");
//            out.print("<td>" + movie.getDuration() + "</td>");
//            out.print("<td>" + movie.getGenre() + "</td>");
//            out.print("<td>" + movie.getDescription() + "</td>");
//            out.print("<td>" + movie.getRating() + "</td>");
//            out.print("<td>" + movie.getStart() + "</td>");
//            out.print("<td>" + movie.getEnd() + "</td>");
//        }
//        out.print("</tr></table></br>");
//
//        out.print("<table width=\"100%\" border=\"1\" cellspacing=\"5\" cellpadding=\"5\">");
//        out.print(" <th>Номер ряда</th>\n" +
//                "        <th>Кол-во мест</th>\n" +
//                "        <th>Номер зала</th>\n"
//        );
//        for (Row row : rowDao.getAll()) {
//            out.print("<tr align = \"center\">");
//            out.print("<td>" + row.getRowNumber() + "</td>");
//            out.print("<td>" + row.getSeatNumber() + "</td>");
//            out.print("<td>" + row.getHall().getId() + "</td>");
//        }
//        out.print("</tr></table>");
//        out.print("</body></html>");
//
//    }
//
//    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        doPost(request,response);
//    }
//}
