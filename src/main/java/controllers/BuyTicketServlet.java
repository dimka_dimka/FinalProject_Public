package controllers;

import dto.TicketDTO;
import dto.UserDTO;
import service.impl.SessionServiceImpl;
import service.impl.TicketServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 08.07.2016.
 */
@WebServlet(name = "BuyTicketServlet", urlPatterns = {"/BuyTicket"})
public class BuyTicketServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /*response.setContentType("text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");*/
        String[] places = request.getParameterValues("check");
        int row = 0;
        int place = 0;
        String[] str = null;
        TicketDTO ticketDTO = null;
        List<TicketDTO> ticketDTOs = new ArrayList<>();
        for (String ticket : places) {
            str = ticket.split("/");
            row = Integer.parseInt(str[0]);
            place = Integer.parseInt(str[1]);
            ticketDTO = new TicketDTO(SessionServiceImpl.getInstance().getById(Integer.parseInt(request.getSession().getAttribute("sessionId").toString())), (UserDTO) request.getSession().getAttribute("user"), row, place, true);
            TicketServiceImpl.getInstance().save(ticketDTO);
            ticketDTOs.add(ticketDTO);
        }

      /*  for (TicketDTO ticket : TicketServiceImpl.getInstance().getAll()) {
            if (ticket.getSession().getId() == Integer.valueOf(request.getSession().getAttribute("sessionId").toString())){
                ticketDTOs.add(ticket);
            }
        }*/
        request.setAttribute("Tickets", ticketDTOs);
        //request.setAttribute("places", places);
        request.getRequestDispatcher("pages/common/BuyTicket.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
