package controllers;

import dto.SessionDTO;
import service.impl.SessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by user on 15.07.2016.
 */
@WebServlet(name = "DeleteSessionServlet", urlPatterns = "/deleteSession")
public class DeleteSessionServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       /* PrintWriter pw = response.getWriter();
        pw.print(request.getParameter("id"));*/
        SessionDTO sessionDTO = SessionServiceImpl.getInstance().getById(Integer.valueOf(request.getParameter("id")));
        SessionServiceImpl.getInstance().delete(sessionDTO.getId());
        request.getSession().setAttribute("message", "Сессия была успешно удалена");
        response.sendRedirect(request.getContextPath() + "/Admin");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
