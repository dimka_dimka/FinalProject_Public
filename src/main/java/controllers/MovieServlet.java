package controllers;

import dto.MovieDTO;
import dto.SessionDTO;
import service.impl.MovieServiceImpl;
import service.impl.SessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dmitr on 09.06.2016.
 */
@WebServlet(name = "MovieServlet", urlPatterns = {"/movie"})
public class MovieServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        MovieDTO movie = MovieServiceImpl.getInstance().getById(Integer.valueOf(request.getParameter("id")));
        List<SessionDTO> sessionDTOList = SessionServiceImpl.getInstance().getAll();
        List<SessionDTO> sessionDTOs = new ArrayList<>();
        for (SessionDTO sessionDTO : sessionDTOList) {
            if (movie.getId() == sessionDTO.getMovie().getId()){
                sessionDTOs.add(sessionDTO);
            }
        }
        request.setAttribute("movie", movie);
        request.setAttribute("sessionDTOs", sessionDTOs);
        request.getRequestDispatcher("pages/common/Movie.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
