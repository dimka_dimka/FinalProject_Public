package controllers;

import dto.HallDTO;
import dto.MovieDTO;
import dto.SessionDTO;
import service.impl.HallServiceImpl;
import service.impl.MovieServiceImpl;
import service.impl.SessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Time;
import java.util.List;

/**
 * Created by user on 15.07.2016.
 */
@WebServlet(name = "AddSessionServlet", urlPatterns = "/addSession")
public class AddSessionServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        MovieDTO movieDTO = MovieServiceImpl.getInstance().getById(Integer.parseInt(request.getSession().getAttribute("idMovie").toString()));
        List<HallDTO> hallDTOs = HallServiceImpl.getInstance().getAll();
        HallDTO hallDTO = null;
        for (HallDTO hall : hallDTOs) {
            if (hall.getName().equals(request.getParameter("hallName"))){
                hallDTO = hall;
            }
        }
        String str = request.getParameter("time");
        String s1 = str.concat(":00");
        Time time = Time.valueOf(s1);
        Double price = Double.valueOf(request.getParameter("price"));
        SessionDTO session = new SessionDTO(movieDTO, time, price, hallDTO);
        SessionServiceImpl.getInstance().save(session);
        request.getSession().setAttribute("message", "Сеанс успешно добавлен");
        response.sendRedirect(request.getContextPath() + "/Admin");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
