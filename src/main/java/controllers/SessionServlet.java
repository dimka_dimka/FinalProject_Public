package controllers;

import dto.HallDTO;
import dto.RowDTO;
import dto.UserDTO;
import service.impl.RowServiceImpl;
import service.impl.SessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 05.07.2016.
 */
@WebServlet(name = "SessionServlet", urlPatterns = {"/session"})
public class SessionServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDTO userDTO = (UserDTO) request.getSession().getAttribute("user");
        request.getSession().setAttribute("sessionId", request.getParameter("id"));
        if (userDTO != null) {
            HallDTO hallDTO = SessionServiceImpl.getInstance().getById(Integer.valueOf(request.getParameter("id"))).getHall();
            List<RowDTO> rows = RowServiceImpl.getInstance().getAll();
            List<RowDTO> rowDTOs = new ArrayList<>();
            for (RowDTO row : rows) {
                if (row.getHall().getId() == hallDTO.getId())
                    rowDTOs.add(row);
            }
            // String[] places = new String[100];
            request.setAttribute("rowDTOs", rowDTOs);
            // request.setAttribute("places", places);
            request.getRequestDispatcher("pages/common/Session.jsp").forward(request, response);
        } else {
            request.getSession().setAttribute("message", "Пожалуйста, авторизируйтесь");
            response.sendRedirect(request.getContextPath() + "/");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
