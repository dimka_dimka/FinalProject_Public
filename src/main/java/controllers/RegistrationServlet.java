package controllers;

import dao.DaoFactory;
import dto.RoleDTO;
import dto.UserDTO;
import mapper.BeanMapper;
import model.Role;
import service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;

/**
 * Created by user on 12.07.2016.
 */
@WebServlet(name = "RegistrationServlet", urlPatterns = {"/registration"})
public class RegistrationServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String firstname = request.getParameter("firstname");
        String lastname = request.getParameter("firstname");
        String email = request.getParameter("email");
        String sex = request.getParameter("sex");
        LocalDate birthday = LocalDate.of(Integer.valueOf(request.getParameter("year")), Integer.valueOf(request.getParameter("month")), Integer.valueOf(request.getParameter("day")));
        Role role = DaoFactory.getInstance().getRoleDao().getBy("role_name", "user");
        RoleDTO roleDTO = BeanMapper.getInstance().singleMapper(role, RoleDTO.class);
        UserDTO userDTO = new UserDTO(login, password, firstname, lastname, email, sex, birthday, roleDTO);
        boolean n = UserServiceImpl.getInstance().existUser(userDTO);
        if (n == false) {
            request.getSession().setAttribute("user", userDTO);
            UserServiceImpl.getInstance().save(userDTO);
            request.getSession().setAttribute("url", request.getRequestURI());
            request.getSession().setAttribute("message", "Регистрация прошла успешно");
           // response.sendRedirect(request.getSession().getAttribute("url").toString());
            response.sendRedirect(request.getContextPath() + "/");
        } else {
            request.getSession().setAttribute("message", "Логин уже существует");
            response.sendRedirect(request.getContextPath() + "/pages/common/registration.jsp");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
