package controllers;

import dto.TicketDTO;
import dto.UserDTO;
import service.impl.TicketServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 13.07.2016.
 */
@WebServlet(name = "UserCabinetServlet", urlPatterns = {"/UserCabinet"})
public class UserCabinetServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDTO userDTO = (UserDTO) request.getSession().getAttribute("user");
        if (userDTO!=null){
            List<TicketDTO> ticketDTOs = new ArrayList<>();
            for (TicketDTO ticket : TicketServiceImpl.getInstance().getAll()) {
                if (ticket.getUser().getId() == userDTO.getId()){
                    ticketDTOs.add(ticket);
                }
            }
            request.setAttribute("Tickets", ticketDTOs);
            request.getRequestDispatcher("pages/common/UserCabinet.jsp").forward(request, response);
        } else {
            request.getSession().setAttribute("message", "Сначала нужно авторизироваться!");
            response.sendRedirect(request.getContextPath() + "/");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
