package controllers;

import dto.MovieDTO;
import service.impl.MovieServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by user on 14.07.2016.
 */
@WebServlet(name = "DeleteMovieServlet", urlPatterns = "/deleteMovie")
public class DeleteMovieServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       MovieDTO movieDTO = (MovieDTO) request.getSession().getAttribute("movie");
   /*     if (movieDTO == null){
            request.getSession().setAttribute("message", "movieDTO = null !!!");
            response.sendRedirect(request.getContextPath() + "/");
        }*/

        MovieServiceImpl.getInstance().delete(movieDTO.getId());
        request.getSession().setAttribute("message", "Movie was succesfull deleted");
        response.sendRedirect(request.getContextPath() + "/Admin");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
