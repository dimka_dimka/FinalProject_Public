package controllers;

import dto.MovieDTO;
import service.impl.MovieServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;

/**
 * Created by user on 15.07.2016.
 */
@WebServlet(name = "AddFilmServlet", urlPatterns = "/addFilm")
public class AddFilmServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = request.getParameter("title");
        String description = request.getParameter("description");
        int duration = Integer.parseInt(request.getParameter("duration"));
        LocalDate rentStart = LocalDate.of(Integer.valueOf(request.getParameter("year")), Integer.valueOf(request.getParameter("month")), Integer.valueOf(request.getParameter("day")));
        LocalDate rentEnd = LocalDate.of(Integer.valueOf(request.getParameter("year2")), Integer.valueOf(request.getParameter("month2")), Integer.valueOf(request.getParameter("day2")));
        String genre = request.getParameter("genre");
        int rating = Integer.parseInt(request.getParameter("rating"));
        MovieDTO movieDTO = new MovieDTO(title, description, duration, rentStart, rentEnd, genre, rating);
        MovieServiceImpl.getInstance().save(movieDTO);
        request.getSession().setAttribute("message", "Фильм успешно добавлен");
        response.sendRedirect(request.getContextPath() + "/Admin");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
