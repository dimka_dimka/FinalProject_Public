package dto;

import model.Entity;

/**
 * Created by dmitr on 13.06.2016.
 */
public class RoleDTO  extends Entity<Integer> {
    private String name;

    public RoleDTO() {
    }

    public RoleDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "RoleDTO{" +
                "name='" + name + '\'' +
                "} " + super.toString();
    }
}