package dto;

import model.Entity;

/**
 * Created by user on 22.06.2016.
 */
public class TicketDTO extends Entity<Integer> {

    private SessionDTO session;
    private UserDTO user;
    private int rowNumber;
    private int seatNumber;
    private boolean isSold;

    public TicketDTO() {
    }

    public TicketDTO(SessionDTO session, UserDTO user, int rowNumber, int seatNumber, boolean isSold) {
        this.session = session;
        this.user = user;
        this.rowNumber = rowNumber;
        this.seatNumber = seatNumber;
        this.isSold = isSold;
    }

    public SessionDTO getSession() {
        return session;
    }

    public void setSession(SessionDTO session) {
        this.session = session;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public int getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(int seatNumber) {
        this.seatNumber = seatNumber;
    }

    public boolean isSold() {
        return isSold;
    }

    public void setSold(boolean sold) {
        isSold = sold;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        TicketDTO ticketDTO = (TicketDTO) o;

        if (rowNumber != ticketDTO.rowNumber) return false;
        if (seatNumber != ticketDTO.seatNumber) return false;
        if (isSold != ticketDTO.isSold) return false;
        if (session != null ? !session.equals(ticketDTO.session) : ticketDTO.session != null) return false;
        return user != null ? user.equals(ticketDTO.user) : ticketDTO.user == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (session != null ? session.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + rowNumber;
        result = 31 * result + seatNumber;
        result = 31 * result + (isSold ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TicketDTO{" +
                "session=" + session +
                ", user=" + user +
                ", rowNumber=" + rowNumber +
                ", seatNumber=" + seatNumber +
                ", isSold=" + isSold +
                '}';
    }
}
