package dto;

import model.Entity;

import java.sql.Time;

/**
 * Created by user on 22.06.2016.
 */
public class SessionDTO extends Entity<Integer> {

    private MovieDTO movie;
    private Time time;
    private double price;
    private HallDTO hall;

    public SessionDTO() {
    }

    public SessionDTO(MovieDTO movie, Time time, double price, HallDTO hall) {
        this.movie = movie;
        this.time = time;
        this.price = price;
        this.hall = hall;
    }

    public MovieDTO getMovie() {
        return movie;
    }

    public void setMovie(MovieDTO movie) {
        this.movie = movie;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public HallDTO getHall() {
        return hall;
    }

    public void setHall(HallDTO hall) {
        this.hall = hall;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        SessionDTO that = (SessionDTO) o;

        if (Double.compare(that.price, price) != 0) return false;
        if (movie != null ? !movie.equals(that.movie) : that.movie != null) return false;
        if (time != null ? !time.equals(that.time) : that.time != null) return false;
        return hall != null ? hall.equals(that.hall) : that.hall == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        result = 31 * result + (movie != null ? movie.hashCode() : 0);
        result = 31 * result + (time != null ? time.hashCode() : 0);
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (hall != null ? hall.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SessionDTO{" +
                "movie=" + movie +
                ", time=" + time +
                ", price=" + price +
                ", hall=" + hall +
                '}';
    }
}
