package dto;

import model.Entity;

/**
 * Created by user on 22.06.2016.
 */
public class RowDTO extends Entity<Integer> {

    private int rowNumber;
    private int seatQuantity;
    private HallDTO hall;

    public RowDTO() {
    }

    public RowDTO(int rowNumber, int seatQuantity, HallDTO hall) {
        this.rowNumber = rowNumber;
        this.seatQuantity = seatQuantity;
        this.hall = hall;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public int getSeatQuantity() {
        return seatQuantity;
    }

    public void setSeatQuantity(int seatQuantity) {
        this.seatQuantity = seatQuantity;
    }

    public HallDTO getHall() {
        return hall;
    }

    public void setHall(HallDTO hall) {
        this.hall = hall;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        RowDTO rowDTO = (RowDTO) o;

        if (rowNumber != rowDTO.rowNumber) return false;
        if (seatQuantity != rowDTO.seatQuantity) return false;
        return hall != null ? hall.equals(rowDTO.hall) : rowDTO.hall == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + rowNumber;
        result = 31 * result + seatQuantity;
        result = 31 * result + (hall != null ? hall.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RowDTO{" +
                "rowNumber=" + rowNumber +
                ", seatQuantity=" + seatQuantity +
                ", hall=" + hall +
                '}';
    }
}
