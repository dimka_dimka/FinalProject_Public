package dto;

import model.Entity;

import java.time.LocalDate;

public class MovieDTO extends Entity<Integer> {

    private String title;
    private String description;
    private int duration;
    private LocalDate rentStart;
    private LocalDate rentEnd;
    private String genre;
    private int rating;

    public MovieDTO() {
    }

    public MovieDTO(String title, String description, int duration, LocalDate rentStart, LocalDate rentEnd, String genre, int rating) {
        this.title = title;
        this.description = description;
        this.duration = duration;
        this.rentStart = rentStart;
        this.rentEnd = rentEnd;
        this.genre = genre;
        this.rating = rating;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public LocalDate getRentStart() {
        return rentStart;
    }

    public void setRentStart(LocalDate rentStart) {
        this.rentStart = rentStart;
    }

    public LocalDate getRentEnd() {
        return rentEnd;
    }

    public void setRentEnd(LocalDate rentEnd) {
        this.rentEnd = rentEnd;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDuration() {
        return duration;
    }

    @Override
    public String toString() {
        return "MovieDTO{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", duration=" + duration +
                ", rentStart=" + rentStart +
                ", rentEnd=" + rentEnd +
                ", genre='" + genre + '\'' +
                ", rating=" + rating +
                "} " + super.toString();
    }
}