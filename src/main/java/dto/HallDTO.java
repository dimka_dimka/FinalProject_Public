package dto;

import model.Entity;

/**
 * Created by user on 22.06.2016.
 */
public class HallDTO extends Entity<Integer> {

    private String name;

    public HallDTO() {
    }

    public HallDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        HallDTO hallDTO = (HallDTO) o;

        return name != null ? name.equals(hallDTO.name) : hallDTO.name == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "HallDTO{" +
                "name='" + name + '\'' +
                '}';
    }
}
