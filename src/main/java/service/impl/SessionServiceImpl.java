package service.impl;

import dao.DaoFactory;
import dao.api.Dao;
import dto.SessionDTO;
import mapper.BeanMapper;
import model.Session;
import service.api.Service;

import java.util.List;

/**
 * Created by user on 22.06.2016.
 */
public class SessionServiceImpl implements Service<Integer, SessionDTO> {

    private static SessionServiceImpl service;
    private Dao<Integer, Session> sessionDao;
    private BeanMapper beanMapper;

    private SessionServiceImpl() {
        sessionDao = DaoFactory.getInstance().getSessionDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized SessionServiceImpl getInstance() {
        if (service == null)
            service = new SessionServiceImpl();
        return service;
    }

    @Override
    public List<SessionDTO> getAll() {
        List<Session> sessions = sessionDao.getAll();
        List<SessionDTO> sessionDTOs = beanMapper.listMapToList(sessions, SessionDTO.class);
        return sessionDTOs;
    }

    @Override
    public SessionDTO getById(Integer id) {
        Session session = sessionDao.getById(id);
        SessionDTO sessionDTO = beanMapper.singleMapper(session, SessionDTO.class);
        return sessionDTO;
    }

    @Override
    public void save(SessionDTO entity) {
        Session session = beanMapper.singleMapper(entity, Session.class);
        sessionDao.save(session);
    }

    @Override
    public void delete(Integer key) {
        sessionDao.delete(key);
    }

    @Override
    public void update(SessionDTO entity) {
        Session session = beanMapper.singleMapper(entity, Session.class);
        sessionDao.update(session);
    }
}
