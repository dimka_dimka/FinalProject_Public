package web;

import dao.DaoFactory;
import dto.UserDTO;
import mapper.BeanMapper;
import model.User;

public class Main {
    public static void main(String[] args) {

 //     UserServiceImpl.getInstance().save(userDTO);
//
//        System.out.println(UserServiceImpl.getInstance().getAll());

      //  System.out.println(DaoFactory.getInstance().getUserDao().getBy("login", "vasya"));

        User user =  DaoFactory.getInstance().getUserDao().getBy("login", "vasya");
        UserDTO userDTO = BeanMapper.getInstance().singleMapper(user, UserDTO.class);
        System.out.println(user);
        System.out.println(userDTO);
    }
}
