package dao.impl;

import model.Hall;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import static dao.SQLs.INSERT_HALL;
import static dao.SQLs.UPDATE_HALL;

/**
 * Created by user on 21.06.2016.
 */
public class HallDaoImpl extends CrudDAO<Hall> {
    public HallDaoImpl() {
        super(Hall.class);
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Hall entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_HALL);
        setStatement(entity, preparedStatement);
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Hall entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_HALL, Statement.RETURN_GENERATED_KEYS);
        setStatement(entity, preparedStatement);
        return preparedStatement;
    }

    public void setStatement (Hall entity, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, entity.getName());
    }

    @Override
    protected List<Hall> readAll(ResultSet resultSet) throws SQLException {
        List<Hall> hallList = new LinkedList<>();
        Hall hall = null;
        while (resultSet.next()){
            hall = new Hall();
            hall.setId(resultSet.getInt("id"));
            hall.setName(resultSet.getString("hall_name"));
            hallList.add(hall);
        }
        return hallList;
    }
}
