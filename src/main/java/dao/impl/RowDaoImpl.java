package dao.impl;

import dao.DaoFactory;
import model.Row;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import static dao.SQLs.INSERT_ROW;
import static dao.SQLs.UPDATE_ROW;

/**
 * Created by user on 21.06.2016.
 */
public class RowDaoImpl extends CrudDAO<Row>{
    public RowDaoImpl() {
        super(Row.class);
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Row entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_ROW);
        setStatement(entity, preparedStatement);
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Row entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_ROW, Statement.RETURN_GENERATED_KEYS);
        setStatement(entity, preparedStatement);
        return preparedStatement;
    }

    private void setStatement(Row entity, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setInt(1, entity.getRowNumber());
        preparedStatement.setInt(2, entity.getSeatQuantity());
        preparedStatement.setInt(3, entity.getHall().getId());
    }

    @Override
    protected List<Row> readAll(ResultSet resultSet) throws SQLException {
        List<Row> rowList = new LinkedList<>();
        Row row = null;
        while (resultSet.next()){
            row = new Row();
            row.setId(resultSet.getInt("id"));
            row.setHall(DaoFactory.getInstance().getHallDao().getById(resultSet.getInt("hall_id")));
            row.setRowNumber(resultSet.getInt("row_number"));
            row.setSeatQuantity(resultSet.getInt("seat_quantity"));
            rowList.add(row);
        }
        return rowList;
    }
}
