package dao.impl;

import dao.DaoFactory;
import model.Session;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import static dao.SQLs.INSERT_SESSION;
import static dao.SQLs.UPDATE_SESSION;

/**
 * Created by user on 20.06.2016.
 */
public class SessionDaoImpl extends CrudDAO<Session> {

    public SessionDaoImpl() {
        super(Session.class);
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Session entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_SESSION);
        setStatement(entity, preparedStatement);
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Session entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_SESSION, Statement.RETURN_GENERATED_KEYS);
        setStatement(entity, preparedStatement);
        return preparedStatement;
    }

    private void setStatement(Session entity, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setTime(1, entity.getTime());
        preparedStatement.setDouble(2, entity.getPrice());
        preparedStatement.setInt(3, entity.getMovie().getId());
        preparedStatement.setInt(4, entity.getHall().getId());
    }

    @Override
    protected List<Session> readAll(ResultSet resultSet) throws SQLException {
        List<Session> sessionList = new LinkedList<>();
        Session session = null;
        while (resultSet.next()){
            session = new Session();
            session.setPrice(resultSet.getDouble("price"));
            session.setId(resultSet.getInt("id"));
            session.setTime(resultSet.getTime("time"));
            session.setMovie(DaoFactory.getInstance().getMovieDao().getById(resultSet.getInt("movie_id")));
            session.setHall(DaoFactory.getInstance().getHallDao().getById(resultSet.getInt("hall_id")));
            sessionList.add(session);
        }
        return sessionList;
    }
}
