package dao.impl;

import dao.DaoFactory;
import model.Ticket;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import static dao.SQLs.INSERT_TICKET;
import static dao.SQLs.UPDATE_TICKET;

/**
 * Created by user on 21.06.2016.
 */
public class TicketDaoImpl extends CrudDAO<Ticket>{
    public TicketDaoImpl() {
        super(Ticket.class);
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Ticket entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_TICKET);
        setStatement(entity, preparedStatement);
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Ticket entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_TICKET, Statement.RETURN_GENERATED_KEYS);
        setStatement(entity, preparedStatement);
        return preparedStatement;
    }

    private void setStatement(Ticket entity, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setInt(1, entity.getSession().getId());
        preparedStatement.setInt(2, entity.getUser().getId());
        preparedStatement.setInt(3, entity.getRowNumber());
        preparedStatement.setInt(4, entity.getSeatNumber());
        preparedStatement.setBoolean(5, entity.isSold());
    }

    @Override
    protected List<Ticket> readAll(ResultSet resultSet) throws SQLException {
        List<Ticket> list = new LinkedList<>();
        Ticket ticket = null;
        while (resultSet.next()){
            ticket = new Ticket();
            ticket.setRowNumber(resultSet.getInt("row_number"));
            ticket.setSeatNumber(resultSet.getInt("seat_number"));
            ticket.setSold(resultSet.getBoolean("is_sold"));
            ticket.setSession(DaoFactory.getInstance().getSessionDao().getById(resultSet.getInt("session_id")));
            ticket.setUser(DaoFactory.getInstance().getUserDao().getById(resultSet.getInt("users_id")));
            list.add(ticket);
        }
        return list;
    }
}
