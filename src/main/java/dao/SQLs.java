package dao;

/**
 * Created by dmitr on 02.06.2016.
 */
public class SQLs {
    public static final String SELECT_ALL = "Select * from %s";
    public static final String FIND_BY_ID = "Select * from %s where id = ?";
    public static final String FIND_BY = "Select * from %s where %s = ?";
    public static final String DELETE_BY_ID = "DELETE FROM %s WHERE id = ?";
    public static  final String INSERT_MOVIE = "Insert into movie (title, description, duration, rent_start, rent_end, genre, rating) values (?,?,?,?,?,?,?)";
    public static  final String UPDATE_MOVIE = "UPDATE movie SET title = ?, description = ?, duration = ?, rent_start = ?, rent_end = ?, genre = ?, rating = ?, WHERE id = ?";

    public static  final String UPDATE_USER = "UPDATE user SET login = ?, password = ?, first_name = ?, last_name = ?, email = ?, sex = ?, birthday = ?, role_id = ?, WHERE id = ?";
    public static  final String INSERT_USER = "Insert into user (login, password, first_name, last_name, email, sex, birthday, role_id) values (?,?,?,?,?,?,?,?)";

    public static final String UPDATE_ROLE = "UPDATE role SET role_name = ?, WHERE id = ?";
    public static final String INSERT_ROLE = "Insert into role (role_name) values (?)";

    public static final String UPDATE_HALL = "UPDATE hall SET hall_name = ?, WHERE id = ?";
    public static final String INSERT_HALL = "Insert into hall (hall_name) values (?)";

    public static  final String UPDATE_SESSION = "UPDATE session SET time = ?, price = ?, movie_id = ?, hall_id = ?, WHERE id = ?";
    public static  final String INSERT_SESSION = "Insert into session (time, price, movie_id, hall_id) values (?,?,?,?)";

    public static final String UPDATE_ROW = "UPDATE row SET row_number = ?, seat_quantity = ?, hall_id = ?, WHERE id = ?";
    public static final String INSERT_ROW = "Insert into row (row_number, seat_quantity, hall_id) values (?,?,?)";

    public static final String UPDATE_TICKET = "UPDATE ticket Set session_id = ?, users_id = ?, row_number = ?, seat_number = ?, is_sold = ?, Where id = ?";
    public static final String INSERT_TICKET = "Insert into ticket (session_id, users_id, row_number, seat_number, is_sold) values (?,?,?,?,?)";

}
