package model;

import java.sql.Time;

/**
 * Created by user on 20.06.2016.
 */
public class Session extends Entity<Integer> {

    private Movie movie;
    private Time time;
    private double price;
    private Hall hall;

    public Session() {
    }

    public Session(Movie movie, Time time, double price, Hall hall) {
        this.movie = movie;
        this.time = time;
        this.price = price;
        this.hall = hall;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Session session = (Session) o;

        if (Double.compare(session.price, price) != 0) return false;
        if (movie != null ? !movie.equals(session.movie) : session.movie != null) return false;
        if (time != null ? !time.equals(session.time) : session.time != null) return false;
        return hall != null ? hall.equals(session.hall) : session.hall == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        result = 31 * result + (movie != null ? movie.hashCode() : 0);
        result = 31 * result + (time != null ? time.hashCode() : 0);
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (hall != null ? hall.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Session{" +
                "movie=" + movie +
                ", time=" + time +
                ", price=" + price +
                ", hall=" + hall +
                '}';
    }
}
