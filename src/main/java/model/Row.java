package model;

/**
 * Created by user on 21.06.2016.
 */
public class Row extends Entity<Integer>{

    private int rowNumber;
    private int seatQuantity;
    private Hall hall;

    public Row() {
    }

    public Row(int rowNumber, int seatQuantity, Hall hall) {
        this.rowNumber = rowNumber;
        this.seatQuantity = seatQuantity;
        this.hall = hall;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public int getSeatQuantity() {
        return seatQuantity;
    }

    public void setSeatQuantity(int seatQuantity) {
        this.seatQuantity = seatQuantity;
    }

    public Hall getHall() {
        return hall;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Row row = (Row) o;

        if (rowNumber != row.rowNumber) return false;
        if (seatQuantity != row.seatQuantity) return false;
        return hall != null ? hall.equals(row.hall) : row.hall == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + rowNumber;
        result = 31 * result + seatQuantity;
        result = 31 * result + (hall != null ? hall.hashCode() : 0);
        return result;
    }

    public void setHall(Hall hall) {
        this.hall = hall;

    }

    @Override
    public String toString() {
        return "Row{" +
                "rowNumber=" + rowNumber +
                ", seatQuantity=" + seatQuantity +
                ", hall=" + hall +
                '}';
    }
}
