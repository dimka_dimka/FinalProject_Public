package model;

/**
 * Created by user on 21.06.2016.
 */
public class Hall extends Entity<Integer> {

    private String name;

    public Hall() {
    }

    public Hall(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Hall hall = (Hall) o;

        return name != null ? name.equals(hall.name) : hall.name == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Hall{" +
                "name='" + name + '\'' +
                '}';
    }
}
