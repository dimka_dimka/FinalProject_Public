package model;

/**
 * Created by user on 21.06.2016.
 */
public class Ticket extends Entity<Integer> {

    private Session session;
    private User user;
    private int rowNumber;
    private int seatNumber;
    private boolean isSold;

    public Ticket() {
    }

    public Ticket(Session session, User user, int rowNumber, int seatNumber, boolean isSold) {
        this.session = session;
        this.user = user;
        this.rowNumber = rowNumber;
        this.seatNumber = seatNumber;
        this.isSold = isSold;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public int getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(int seatNumber) {
        this.seatNumber = seatNumber;
    }

    public boolean isSold() {
        return isSold;
    }

    public void setSold(boolean sold) {
        isSold = sold;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Ticket ticket = (Ticket) o;

        if (rowNumber != ticket.rowNumber) return false;
        if (seatNumber != ticket.seatNumber) return false;
        if (isSold != ticket.isSold) return false;
        if (session != null ? !session.equals(ticket.session) : ticket.session != null) return false;
        return user != null ? user.equals(ticket.user) : ticket.user == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (session != null ? session.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + rowNumber;
        result = 31 * result + seatNumber;
        result = 31 * result + (isSold ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "session=" + session +
                ", user=" + user +
                ", rowNumber=" + rowNumber +
                ", seatNumber=" + seatNumber +
                ", isSold=" + isSold +
                '}';
    }
}
